package com.test.micro2;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@RequiredArgsConstructor
public class User implements Serializable {

    @Id
    private Long id;

    private String username;

    private int age;

    private LocalDateTime createdAt;
}
