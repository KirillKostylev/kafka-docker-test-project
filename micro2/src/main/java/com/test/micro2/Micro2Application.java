package com.test.micro2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Micro2Application {

	public static void main(String[] args) {
		SpringApplication.run(Micro2Application.class, args);
	}


//	@Bean
//	public NewTopic createTopic(){
//		return new NewTopic(TOPIC,3,(short) 1);
//	}
}
