package com.test.micro2;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserKafkaService {
    private static final String TOPIC = "user_test_topic";

    private final UserRepository userRepository;

    //    @KafkaListener(topics = TOPIC)
    public void saveUser(User user) {
        log.info("---GOT USER --- {}", user);
        User save = userRepository.save(user);
        log.info("---SAVED USER --- {}", save);
    }

    public User findById(Long id) {
        log.info("---FIND BY ID {}---", id);
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("User not found " + id));
        log.info("---FOUND USER ---{}", user);
        return user;
    }

    public List<User> findAll() {
        log.info("---GET ALL USERS FROM POSTGRES ---");
        return userRepository.findAll();
    }

}
