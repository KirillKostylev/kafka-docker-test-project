package com.test.micro1.micro1.repository;

import com.test.micro1.micro1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
