package com.test.micro1.micro1.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@RequiredArgsConstructor
@Table(name = "users")
public class User implements Serializable {

    @SequenceGenerator(name = "USER_ID_NAME", sequenceName = "USER_ID_SEQ_NAME")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ID_NAME")
    @Id
    private Long id;

    private String username;

    private int age;

    @CreationTimestamp
    private LocalDateTime createdAt;
}
