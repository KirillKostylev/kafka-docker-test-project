package com.test.micro1.micro1.service;

import com.test.micro1.micro1.entity.User;
import com.test.micro1.micro1.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {
    private static final String TOPIC = "user_test_topic";
    public static final String U_2_USERS = "/m2/users/";

    @Value("${spring.customer.resttemplate.url}")
    private String customerUrl;

    private final UserRepository userRepository;
//    private final KafkaTemplate<Long, Object> kafkaTemplate;

    public User saveAndReportToService(User user){
        log.info("---GET USER REQUEST---{}", user);
        User savedUser = save(user);
        log.info("---SAVED USER IN POSTGRES---{}", savedUser);

//        kafkaTemplate.send(TOPIC, savedUser.getId(), savedUser);

//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.postForEntity(customerUrl + U_2_USERS,savedUser,User.class);

        return savedUser;
    }

    private User save(User user){
        return userRepository.save(user);
    }

    public List<User> findAll(){
        log.info("---GET ALL USERS FROM POSTGRES ---");
        return userRepository.findAll();
    }

    public User findById(Long id){
        log.info("---GET USER FROM POSTGRES BY ID={}---", id);
        return userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("User not found " + id));
    }
}
