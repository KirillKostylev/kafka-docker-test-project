package com.test.micro1.micro1.controller;

import com.test.micro1.micro1.entity.User;
import com.test.micro1.micro1.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("m1/users")
public class UserController {

    private final UserService userService;

    @PostMapping
    public User save(@RequestBody User user) {
        return userService.saveAndReportToService(user);
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @GetMapping
    public List<User> findAll() {

        String a = "aqdfghnjdsavfutweavchwbncweaqfc";
        Map<String, Integer> collect = Arrays.stream(a.split("")).collect(Collectors.toMap(c -> c, c -> 1, Integer::sum));
        String key = collect.entrySet().stream().max(Comparator.comparingInt(Entry::getValue)).get().getKey();
        return userService.findAll();
    }
}
